# Blockchain-presentation


## Example of DEX (decentralized exchanges)
https://app.uniswap.org/
https://pancakeswap.finance/

## Example of CEX (centralized exchanges)
https://www.binance.com/
https://www.coinbase.com/

## Follow crypto prices
https://coinmarketcap.com/

## Blockchain developer roadmap
https://roadmap.sh/blockchain

## Latech website
https://latech.io/

